# ruby-kata-csv-viewer

Original Source: https://ccd-school.de/coding-dojo/agility-katas/viewing-csv-files-i/


## Viewing CSV Files I
Agility Kata „Viewing CSV Files I“
Write an application to view CSV files on the console. [1]It ́s supposed to be a very simple program: just call it from the command line like this
```
C:\>csvviewer.exe persons.csv
```
…and it will display the file ́s content in a page wise manner like that: [2]
```
Name |Age|City    |
-----+---+--------+
Peter|42 |New York|
Paul |57 |London  |
Mary |35 |Munich  |
F)irst page, P)revious page, N)ext page, L)ast page, E)xit
```

Pages of data records are displayed as a table with a header line and cell borders like depicted. The corresponding file looks like follows; when assuming a page length of 3 data records it would be displayed as three pages:
```
Name;Age;City
Peter;42;New York
Paul;57;London
Mary;35;Munich
Jaques;66;Paris
Yuri;23;Moscow
Stephanie;47;Stockholm
Nadia;29;Madrid
```
Each line contains a data record; the first line contains the column names; columns are separated by „;“. The data record fields’ content is simple: no line breaks, no field separators, no need to process values put in „“. The encoding of the CSV text files is UTF-8.

The page length should have a reasonable default for your platform ́s console windows, but it also can be set by passing it to the application like this:
```
C:\>csvviewer.exe persons.csv 40
```
The page table should have fixed width columns matching the longest value for each column within a page.

[1] A console frontend is used for this exercise because that ́s the least common denominator in terms of frontend for all platforms. Whether you ́re doing Ruby or C# or Java you should be able to put a console frontend on top of any business logic. Sure a GUI would be more stylish, but it also would distract from the main challenges of the exercise.

[2] Assume all CSV files to contain only lines of a length fitting a display line; no horizontal scrolling needed. Very likely all lines are of different length, though.